# Chu Playground

There is yet no other aim for this project than playing around with Chu spaces.  

## Chu Space

[Chu spaces on Boole.Stanford.EDU](http://boole.stanford.edu/chuguide.html)
> A Chu space is a transformable matrix whose rows transform forwards while its columns transform backwards.  

For now I'm just converting the [code](http://chu.stanford.edu/source/) until I can use it.  


## Run

First install `ts-node` globally  
```
npm install -g ts-node
```

Then install package dependencies  
```
npm install
```

You are now able to run the code, for instance if you just want to run the demo in `index.ts` :
```
ts-node .
```