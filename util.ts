export type Tuple<T, N extends number = number> = T[] & { length: N };

export type Matrix<T, R extends number = number, C extends number = number> = Tuple<Tuple<T, C>, R>;

/* 'Sum' below doesn't work as expected, it simply reduces to type 'number'.  
 * 'Prod' is much more complicated anyway, so both are 'number's for now.  
 */
export type Sum<A extends number, B extends number> = [...Tuple<unknown, A>, ...Tuple<unknown, B>]["length"];
export type Prod<A extends number, B extends number> = number;

export const _throw = (x: unknown): never => { throw x; };

export const isError = (x: unknown): x is Error => x instanceof Error;
export const isNotError = <T>(x: T | Error): x is T => !isError(x);
export const noError = <T>(x: (T | Error)[]): x is T[] => !x.some(isError);

export const throwIfError = <T>(x: T | Error): T => isError(x) ? _throw(x) : x;
export const throwIfSomeError = <T>(x: (T | Error)[]): T[] => noError(x) ? x : _throw(x.filter(isError));