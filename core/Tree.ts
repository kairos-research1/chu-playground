import Link from "./Link";
import TreeNode from "./TreeNode";

/** A tree is used to store a collection of equal-length "lines".  
 *  The lines are sequences of integers in the range 0..arity-1  
 */
export default class Tree {
  public top: TreeNode = new TreeNode(null, 0);

  /* constructor */
  constructor(
    public arity: number,
    public length: number,
  ) { }

  /** Returns a linked list of the indexes of all lines matching the given line. */
  findLine(line: number[]): Link | null {
    if (line.length !== this.length) return null;

    let current: TreeNode | null = this.top;

    for (let loc = 0; loc < line.length; loc++) {
      current = current?.child(line[loc]) ?? null;
      if (current === null) return null;
    }
    return current.data;
  }

  /* mutator */

  /** Inserts the given line at the given index.  
   *  Returns a linked list of the indexes of all other lines which match the new line. 
  */
  addLine(line: number[], index: number): Link | null {
    if (line.length !== this.length) return null;

    let current = this.top;
    for (let loc = 0; loc < line.length; loc++)
      current = current.grow(line[loc], this.arity);

    const result = current.data;
    current.addDatum(index);
    return result;
  }

  /** for debugging purposes only */
  show(): void {
    console.log("ARITY " + this.arity);
    console.log("LENGTH " + this.length);
    this.top.show("");
  }
}
