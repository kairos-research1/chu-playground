export default class Context {
    constructor(
        private k: number,
        public standardization: boolean,
    ) { };
}
