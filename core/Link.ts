export default class Link {
  constructor(
    public next: Link | null,
    public datum: number,
  ) { }

  /** for debugging purposes only */
  show(pad: string): void {
    console.log(pad + this.datum);
    this.next?.show(pad);
  }
}
