import { Matrix, Prod, Sum, Tuple } from "../util";

import Conformable from "./Conformable";
import Tree from "./Tree";
import MatrixGenerator from "./MatrixGenerator";
import Context from "./Context";

/** A Chu space is a matrix with entries drawn from a set of some finite size K.  
 *  The members of the set are represented by numbers in [0,K-1].  
 */
export default class Chu<K extends number = number, R extends number = number, C extends number = number> implements Conformable {
    private standard: Chu<K> | null;

    /** Trusting constructor: performs no consistency checks */
    private constructor(
        public K: K,
        public nrows: R,
        public ncols: C,
        private matrix: Matrix<number, R, C>,
        standardized: boolean,
    ) {
        this.standard = (standardized ? this : null);
    }

    /** Special constructor: builds tensor unit */
    static unit<L extends number>(length: L): Chu<L, 1, L> {
        return new Chu(
            length, 1, length,
            [
                Array.from({ length }, ((_, i) => i))
            ] as Matrix<number, 1, L>,
            true
        );
    }

    /** Special constructor: builds zero tensor */
    static zero(): Chu<0, 0, 0> {
        return new Chu(0, 0, 0, [] as Matrix<number, 0, 0>, false);
    }

    /** Parse constructor: builds a Chu space from the given Strings.  
     *  Rows are newline terminated, other whitespace is ignored.  
     *  Entries are represented by digits 0..K-1 ( K<=10 ).  
     *  A SyntaxError is thrown to report trouble.  
     */
    static parse(text: string, kText = "", rowText = "", colText = ""): Chu | SyntaxError {
        const chu: Chu = Chu.zero();

        // Initialize K, nrows, ncols

        let kInit = false;
        if (kText) {
            kText = kText.trim();
            try {
                chu.K = parseInt(kText);
                if (chu.K < 0 || chu.K > 10) return new SyntaxError("K=" + chu.K + " is out of bounds.  Use 0<=K<=10");
                else kInit = true;
            }
            catch (err) {
                if (kText.length !== 0) return new SyntaxError("can't parse K='" + kText + "'");
                else throw err;
            }
        }

        let nrowsInit = false;
        if (rowText) {
            rowText = rowText.trim();
            try {
                chu.nrows = parseInt(rowText);
                if (chu.nrows < 0) return new SyntaxError("#rows=" + chu.nrows + " is out of bounds.  Use nrows>=0");
                else nrowsInit = true;
            }
            catch (err) {
                if (rowText.length !== 0) return new SyntaxError("can't parse #rows='" + rowText + "'");
                else throw err;
            }
        }

        let ncolsInit = false;
        if (colText) {
            colText = colText.trim();
            try {
                chu.ncols = parseInt(colText);
                if (chu.ncols < 0) return new SyntaxError("#cols=" + chu.ncols + " is out of bounds.  Use ncols>=0");
                else ncolsInit = true;
            }
            catch (err) {
                if (colText.length !== 0) return new SyntaxError("can't parse #cols='" + colText + "'");
                else throw err;
            }
        }

        // cut trailing whitespace, esp. newlines
        text = text.trim();

        // Set up rowTokenizer
        // StringTokenizer rowTokenizer = new StringTokenizer(text, "\n");
        const rows = text.split("\n");

        // Infer nrows if not already initialized
        if (!nrowsInit) {
            // c.nrows = rowTokenizer.countTokens();
            chu.nrows = rows.length;
            nrowsInit = true;
        }

        // Set up matrix
        chu.matrix = Array.from({ length: chu.nrows }, () => ([]));

        // Build rows from rowTokenizer: store results in matrix
        let r = 0;
        for (; r < chu.nrows && r < rows.length; r++) {

            const rowString: string = rows[r];
            const entries: number[] = [];

            // put all the entries in this row into the Vector entries 
            for (const ch of rowString) {
                if ((ch == ' ' || ch == '\t')) continue; // skip white space

                const d = parseInt(ch, 10);
                if (!isNaN(d)) {
                    if (kInit) {
                        if (d < chu.K) entries.push(d);
                        else return new SyntaxError(
                            "Entry (" + (r + 1) + "," +
                            (entries.length + 1) + ")" +
                            " out of bounds: " +
                            d + " > K-1=" + (chu.K - 1)
                        );
                    }
                    else { // !kInit
                        entries.push(d);
                        if (d >= chu.K) chu.K = d + 1;
                    }
                }
                else { // isNaN
                    const c = entries.length;
                    return new SyntaxError(
                        "Entry (" + (r + 1) + "," + (c + 1) + ")" +
                        "=`" + rowString.substring(c, c + 1) +
                        "' is not a digit"
                    );
                }
            }

            // Infer ncols if not already initialized
            // Set up matrix[r]

            if (!ncolsInit) {
                chu.ncols = entries.length;
                ncolsInit = true;
            }
            // copy ncols elements into matrix[r]: pad as needed
            chu.matrix[r] = Array.from({ length: chu.ncols }, (_, c) => entries[c] ?? 0);
        }

        // Pad matrix with rows of zeros as needed
        for (; r < chu.nrows; r++) {
            chu.matrix[r] = Array.from({ length: chu.ncols }, () => 0);
        }

        return chu;
    }

    /** Returns a string representing this space.
     *  Errors are handled by throwing a TypeError
     */
    serialize(): string | TypeError {
        if (this.K > 10) return new TypeError("K=" + this.K + " is out of bounds");
        return this.matrix.map((r) => r.join("")).join("\n");
    }

    rowTree(): Tree {
        const result = new Tree(this.K, this.ncols);
        for (let r = 0; r < this.nrows; r++) { // Loop over rows  
            result.addLine(this.matrix[r], r); // add row to tree  
        }
        return result;
    }

    colTree(): Tree {
        const result = new Tree(this.K, this.nrows);
        for (let c = 0; c < this.ncols; c++) { // Loop over columns
            result.addLine(Array.from({ length: this.nrows }, (_, r) => this.matrix[r][c]), c); // add column to tree
        }
        return result;
    }

    /* Unary Operations */

    dual(): Chu<K, C, R> {
        return new Chu<K, C, R>(
            this.K, this.ncols, this.nrows,
            Array.from({ length: this.ncols }, (_, c) =>
                Array.from({ length: this.nrows }, (_, r) =>
                    this.matrix[r][c]
                )
            ) as Matrix<number, C, R>,
            this.standard === this // dual is standard iff original is
        );
    }

    /** The rows of ?A are closed under the following operation:  
     *  Form a square matrix whose rows and columns are rows of A, and build a new row from the diagonal.  
     *  The implementation below simply performs this operation repeatedly until there is nothing new generated.  
     */
    query(): Chu<K, number, C> {
        if (this.K === 2) return this.query2() as Chu<K, number, C>;

        // The final number of rows is unknown, so for now hold them in a Vector.
        const result_rows: Matrix<number, number, C> = [];

        // row_tree holds the same rows as result_rows.  
        // (the Tree form is useful for feeding the MatrixGenerator)  
        const row_tree = this.rowTree();

        // ?A must contain all constant rows
        for (let k = 0; k < this.K; k++) {
            const const_row = Array.from({ length: this.ncols }, () => k) as Tuple<K, C>;
            if (row_tree.findLine(const_row) === null) {
                // This constant row is new!
                row_tree.addLine(const_row, result_rows.length);
                result_rows.push(const_row);
            }
        }

        while (true) {
            // Build all diagonals and put them in future_rows
            const future_rows = [] as Tuple<Tuple<number, C>>;
            const MG = new MatrixGenerator(row_tree, row_tree);
            while (MG.next()) future_rows.push(Array.from({ length: this.ncols },
                (_, i) => result_rows[MG.rowLinks[i]?.datum ?? 0][i]
            ) as Tuple<number, C>);

            // Search future_rows for new rows.  
            // Add new rows to row_tree, result_rows.  
            // If none of the rows are new, break the loop.  
            let done = true;
            future_rows.forEach((row) => {
                if (!row_tree.findLine(row)) {
                    // This row is new!
                    done = false;
                    row_tree.addLine(row, result_rows.length);
                    result_rows.push(row);
                }
            });
            if (done) break;
        }

        // All the rows have been generated: now build the result
        return new Chu(this.K, result_rows.length, this.ncols, result_rows, false);
    }

    /** Closes the rows of A under union and instersection. */
    query2(): Chu<2, number, C> {
        // The final number of rows is unknown, so for now hold them in a Vector.
        const result_rows: Matrix<number, number, C> = [];

        // row_tree holds the same rows as result_rows.  
        // (The purpose of the Tree is simply to make checking for duplicates faster)  
        const row_tree = new Tree(2, this.ncols);

        // Put all the rows of original space on the stack
        const future_rows: Matrix<number, number, C> = this.matrix.slice(this.nrows);
        // Don't forget the union and intersection of the empty set of rows:
        future_rows.push(Array.from({ length: this.ncols }, () => 0) as Tuple<number, C>);
        future_rows.push(Array.from({ length: this.ncols }, () => 1) as Tuple<number, C>);

        // Loop until no rows remain to insert 
        while (future_rows.length) {
            const row = future_rows.pop();
            if (row && !row_tree.findLine(row)) {
                result_rows.forEach((old_row) => {
                    future_rows.push(Array.from({ length: this.ncols }, (_, c) => (((row[c] == 1) || (old_row[c] == 1)) ? 1 : 0)) as Tuple<number, C>);
                    future_rows.push(Array.from({ length: this.ncols }, (_, c) => (((row[c] == 1) && (old_row[c] == 1)) ? 1 : 0)) as Tuple<number, C>);
                });

                // Add row to the result
                row_tree.addLine(row, result_rows.length);
                result_rows.push(row);
            }
        }

        // All the rows have been generated: now build the result
        return new Chu(2, result_rows.length, this.ncols, result_rows, false);
    }

    /* Binary operations */

    static choice<
        AK extends number, AR extends number, AC extends number,
        BK extends number, BR extends number, BC extends number
    >(A: Chu<AK, AR, AC>, B: Chu<BK, BR, BC>): Chu<AK | BK, Sum<AR, BR>, Sum<AC, BC>> {
        const K = Math.max(A.K, B.K) as AK | BK;
        const nrows = A.nrows + B.nrows as Sum<AR, BR>;
        const ncols = A.ncols + B.ncols as Sum<AC, BC>;

        const matrix = Array.from({ length: nrows }, (_, r) => Array.from({ length: ncols }, (_, c) =>
            r < A.nrows
                ? c < A.ncols ? A.matrix[r][c] : 0
                : c < A.ncols ? 0 : B.matrix[r - A.nrows][c - A.ncols]
        )) as Matrix<number, typeof nrows, typeof ncols>;

        return new Chu(K, nrows, ncols, matrix, false);
    }

    static product<
        AK extends number, AR extends number, AC extends number,
        BK extends number, BR extends number, BC extends number
    >(A: Chu<AK, AR, AC>, B: Chu<BK, BR, BC>): Chu<AK | BK, Prod<AR, BR>, Sum<AC, BC>> {
        const K = Math.max(A.K, B.K) as AK | BK;
        const nrows = A.nrows * B.nrows as Prod<AR, BR>;
        const ncols = A.ncols + B.ncols as Sum<AR, BR>;

        const matrix = Array.from({ length: nrows }, (_, ar) => Array.from({ length: ncols })) as Matrix<number, typeof nrows, typeof ncols>;

        let r = 0, c = 0;
        for (let ar = 0; ar < A.nrows; ar++) { // Loop over rows of A
            for (let br = 0; br < B.nrows; br++) { // Loop over rows of B
                // Create concatination of A.matrix[ar] and B.matrix[br]

                for (let ac = 0; ac < A.ncols; ac++)
                    matrix[r][c++] = A.matrix[ar][ac];

                for (let bc = 0; bc < B.ncols; bc++)
                    matrix[r][c++] = B.matrix[br][bc];

                r++; c = 0;
            }
        }

        return new Chu(K, nrows, ncols, matrix, false);
    }

    static sequence<
        AK extends number, AR extends number,
        BK extends number, BR extends number
    >(A: Chu<AK, AR>, B: Chu<BK, BR>): Chu<AK | BK, Sum<AR, BR>> {
        const K = Math.max(A.K, B.K) as AK | BK;

        // Classify columns of A and B

        const classificationA = A.classifyCols();
        const classificationB = B.classifyCols();

        // Count rows and columns of answer.  
        //   A column of the answer consists of the concatination of  
        // a column of A and a column of B.  Duplicates are not allowed.  
        // The column (state) of A must be final   (= FINAL   || UNKNOWN).  
        // The column (state) of B must be initial (= INITIAL || UNKNOWN).  

        const nrows = A.nrows + B.nrows as Sum<AR, BR>;
        let ncols = 0;

        for (let ac = 0; ac < A.ncols; ac++) { // Loop over cols of A
            if (classificationA[ac] === ColClass.DUPLICATE) continue;

            for (let bc = 0; bc < B.ncols; bc++) { // Loop over cols of B
                if (classificationB[bc] === ColClass.DUPLICATE) continue;

                if ((classificationA[ac] === ColClass.UNKNOWN) ||
                    (classificationA[ac] === ColClass.FINAL) ||
                    (classificationB[bc] === ColClass.UNKNOWN) ||
                    (classificationB[bc] === ColClass.INITIAL)
                ) {
                    ncols++;
                }
            }
        }

        // Form answer, column by column

        let matrix = Array.from({ length: nrows }, () => Array.from({ length: ncols })) as Matrix<number, typeof nrows, typeof ncols>;
        let r = 0; let c = 0;

        for (let ac = 0; ac < A.ncols; ac++) { // Loop over cols of A
            if (classificationA[ac] == ColClass.DUPLICATE) continue;

            for (let bc = 0; bc < B.ncols; bc++) { // Loop over cols of B
                if (classificationB[bc] == ColClass.DUPLICATE) continue;

                if ((classificationA[ac] == ColClass.UNKNOWN) ||
                    (classificationA[ac] == ColClass.FINAL) ||
                    (classificationB[bc] == ColClass.UNKNOWN) ||
                    (classificationB[bc] == ColClass.INITIAL)) {
                    // Create concatination of A.matrix[*][ac] and B.matrix[*][bc]

                    for (let ar = 0; ar < A.nrows; ar++)
                        matrix[r++][c] = A.matrix[ar][ac];

                    for (let br = 0; br < B.nrows; br++)
                        matrix[r++][c] = B.matrix[br][bc];

                    r = 0; c++;
                }
            }
        }

        // Build and return result
        return new Chu(K, nrows, ncols, matrix, false);
    }

    /** Returns an array of integers which classify the columns of a Chu space into the five catagories above. */
    public classifyCols(): Tuple<ColClass, C> {
        const classification = Array.from({ length: this.ncols }) as Tuple<ColClass, C>;

        OUTER: for (let c = 0; c < this.ncols; c++) {
            classification[c] = ColClass.UNKNOWN;

            INNER: for (let d = 0; d < c; d++) {
                // skip comparisons against duplicates or middle elements.
                switch (classification[d]) {
                    case ColClass.DUPLICATE:
                    case ColClass.MIDDLE:
                        continue INNER;
                }

                switch (this.compareCols(c, d)) {
                    // col c <> col d, so nothing can be infered
                    case CompClass.IC:
                        continue INNER;

                    // col c == col d, throw out c by classifying it as DUPLICATE.
                    case CompClass.EQ:
                        classification[c] = ColClass.DUPLICATE;
                        continue OUTER;

                    // col c < col d.  
                    case CompClass.LT:
                        switch (classification[c]) {
                            case ColClass.UNKNOWN: classification[c] = ColClass.INITIAL; break;
                            case ColClass.FINAL: classification[c] = ColClass.MIDDLE; break;
                        }
                        switch (classification[d]) {
                            case ColClass.UNKNOWN: classification[d] = ColClass.FINAL; break;
                            case ColClass.INITIAL: classification[d] = ColClass.MIDDLE; break;
                        }
                        break;

                    // col c > col d.
                    case CompClass.GT:
                        switch (classification[c]) {
                            case ColClass.UNKNOWN: classification[c] = ColClass.FINAL; break;
                            case ColClass.INITIAL: classification[c] = ColClass.MIDDLE; break;
                        }
                        switch (classification[d]) {
                            case ColClass.UNKNOWN: classification[d] = ColClass.INITIAL; break;
                            case ColClass.FINAL: classification[d] = ColClass.MIDDLE; break;
                        }
                        break;
                }
            } // INNER
        } // OUTER
        return classification;
    }

    /** Compares two columns componentwise and returns one of the four code values above. */
    private compareCols(col1: number, col2: number): CompClass {
        let result = CompClass.EQ;

        for (let r = 0; r < this.nrows; r++) {
            if (this.matrix[r][col1] == this.matrix[r][col2]) {
                continue;
            }
            else if (this.matrix[r][col1] < this.matrix[r][col2]) {
                switch (result) {
                    case CompClass.EQ: result = CompClass.LT; break;
                    case CompClass.GT: return CompClass.IC;
                }
            }
            else {
                switch (result) {
                    case CompClass.EQ: result = CompClass.GT; break;
                    case CompClass.LT: return CompClass.IC;
                }
            }
        }

        return result;
    }

    static implication<
        AK extends number, AR extends number,
        BK extends number, BC extends number
    >(A: Chu<AK, AR, number>, B: Chu<BK, number, BC>): Chu<AK | BK> {
        const K = Math.min(A.K, B.K) as AK | BK;

        // The "rows" of implication are Chu transforms from A to B
        // These transforms consist of matrices that are ambigiously
        // composed of columns of A or rows of B.  Thus the size of
        // these rows/transforms/matrices is: 
        const size = A.nrows * B.ncols as Prod<AR, BC>;

        // The number of transforms is not known in advance, so
        // for now they will go in a variable-length Vector:
        const transforms = [] as Matrix<number>;

        // Build the MatrixGenerator, using prefix trees
        // of the possible rows and columns of the matrix:
        const MG = new MatrixGenerator(B.rowTree(), A.colTree());

        while (MG.next()) {
            // Count instances of this matrix.
            // Whenever there are multiple choices for a row or column,
            // the number of instances is multiplied.
            let num_instances = 1;
            for (let r = 0; r < MG.nrows; r++) {
                let l = MG.rowLinks[r];
                let length = 0;
                while (l) {
                    l = l.next;
                    length++;
                }
                num_instances *= length;
            }
            for (let c = 0; c < MG.ncols; c++) {
                let l = MG.colLinks[c];
                let length = 0;
                while (l) {
                    l = l.next;
                    length++;
                }
                num_instances *= length;
            }

            // Build the current transform
            const transform = Array.from({ length: size }) as Tuple<number, typeof size>;
            for (let r = 0; r < MG.nrows; r++)
                for (let c = 0; c < MG.ncols; c++) {
                    const row_index = MG.rowLinks[r]?.datum ?? 0;
                    const row = B.matrix[row_index];
                    const entry = row[c];
                    transform[r * MG.ncols + c] = entry;
                }

            // Record the transform 
            for (let i = 0; i < num_instances; i++) {
                transforms.push(transform);
            }
        }

        return new Chu(K, transforms.length, size, transforms, false);
    }

    /* Conformable interface */

    public conform(context: Context): Chu<K> {
        if (context.standardization) return this.standardize();
        else return this;
    }

    private standardize(): Chu<K> {
        // If the standard version of this space is not known,
        // compute it and keep a pointer to it.
        if (!this.standard) {
            // new_nrows counts non-repeat rows
            // unique_rows[] contains indexes of non-repeat rows;
            // (Similarly for cols)
            let unique_rows = Array.from({ length: this.nrows }) as Tuple<number, R>;
            let unique_cols = Array.from({ length: this.ncols }) as Tuple<number, C>;
            let new_nrows = this.row_sort(unique_rows);
            let new_ncols = this.col_sort(unique_cols);

            if ((this.nrows === new_nrows) && (this.ncols === new_ncols)) { // Already standardized!
                this.standard = this;
            }
            else { // Build the standardized version
                const new_matrix = Array.from({ length: new_nrows }, (_, r) => Array.from({ length: new_ncols }, (_, c) =>
                    this.matrix[unique_rows[r]][unique_cols[c]]
                ));

                this.standard = new Chu(this.K, new_nrows, new_ncols, new_matrix, true);
            }
        }

        return this.standard;
    }

    private row_sort(unique_rows: number[]): number {
        /* Record(unique_rows) and count(num_unique) all unique rows.
         * Throw out all copies.
         */
        let num_unique = 0;
        sort: for (let r = 0; r < this.nrows; r++) {

            /* Look for row r in the current set of unique rows.
             * If row r is not a copy, insert it into the set.
             * l,h mark bounds of possible insertion locations
             */
            let l = 0, h = num_unique;
            search: while (l < h) {

                /* Does row unique_rows[m] match row r?
                 */
                const m = (l + h) / 2;
                compare: for (let c = 0; c < this.ncols; c++) {

                    /* scan quickly for differences
                     */
                    if (this.matrix[unique_rows[m]][c] == this.matrix[r][c])
                        continue compare;

                    /* row unique_rows[m] does not match row r.
                     * narrow range and continue search.
                     */
                    if (this.matrix[unique_rows[m]][c] > this.matrix[r][c])
                        h = m;
                    else
                        l = m + 1;
                    continue search;

                } // end compare

                /* If we get here, we have a match.
                 * Throw out row r 
                 */
                continue sort;

            } // end search

            /* We have a new row.  Insert it! 
             */
            for (let i = num_unique; i > l; i--)
                unique_rows[i] = unique_rows[i - 1];
            unique_rows[l] = r;
            num_unique++;

        } // end sort

        return num_unique;
    }

    private col_sort(unique_cols: number[]): number {
        /* Record (in unique_cols) and count (in num_unique) all unique cols.
         * Throw out all copies.
         */
        let num_unique = 0;
        sort: for (let c = 0; c < this.ncols; c++) {

            /* Look for col c in the current set of unique cols.
             * If col c is not a copy, insert it into the set.
             * l,h mark bounds of possible insertion locations
             */
            let l = 0, h = num_unique;
            search: while (l < h) {

                /* Does col unique_cols[m] match col c?
                 */
                const m = (l + h) / 2;
                compare: for (let r = 0; r < this.nrows; r++) {

                    /* scan quickly for differences
                     */
                    if (this.matrix[r][unique_cols[m]] == this.matrix[r][c])
                        continue compare;

                    /* col unique_rcols[m] does not match col c.
                     * narrow range and continue search.
                     */
                    if (this.matrix[r][unique_cols[m]] > this.matrix[r][c])
                        h = m;
                    else
                        l = m + 1;
                    continue search;

                } // end compare

                /* If we get here, we have a match.  
                 * Throw out col c  
                 */
                continue sort;

            } // end search

            /* We have a new col.  Insert it!  
             */
            for (let i = num_unique; i > l; i--)
                unique_cols[i] = unique_cols[i - 1];
            unique_cols[l] = c;
            num_unique++;

        } // end sort

        return num_unique;
    }
};

export enum ColClass {
    /** nothing < x < nothing */
    UNKNOWN = 0,
    /** nothing < x < something */
    INITIAL = 1,
    /** something < x < nothing */
    FINAL = 2,
    /** something < x < something */
    MIDDLE = 3,
    /** x = previous something */
    DUPLICATE = 4,
}

export function printColClass(c: ColClass): string {
    return {
        [ColClass.UNKNOWN]: "?",
        [ColClass.INITIAL]: "I",
        [ColClass.FINAL]: "F",
        [ColClass.MIDDLE]: "M",
        [ColClass.DUPLICATE]: "D",
    }[c];
}

export enum CompClass {
    /** Equal = */
    EQ = 0,
    /** Less than < */
    LT = 1,
    /** Greater than > */
    GT = 2,
    /** Incomparable ⊥ */
    IC = 3,
}

export function printCompClass(c: CompClass): string {
    return {
        [CompClass.EQ]: "=",
        [CompClass.LT]: "<",
        [CompClass.GT]: ">",
        [CompClass.IC]: "⊥",
    }[c];
}