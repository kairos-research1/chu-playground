import Link from "./Link";

/** A Node (of a Tree) represents a given prefix in a set of lines.  
 *  The children of a node are the possible extensions of that prefix.  
 *  A Node consists of:  
 *  - an array of child Nodes (for non-leaves)  
 *  - a List of line indexes (for leaves)  
 *  - a pointer to the parent Node (representing this prefix sans last element)  
 *  - a branch number (the value of the last element)   
 */
export default class TreeNode {
  public children: TreeNode[] = [];
  public data: Link | null = null;

  /* constructors */
  constructor(
    public parent: TreeNode | null,
    public branch: number,
  ) { }

  child(i: number): TreeNode | null {
    return this.children[i] ?? null;
  }

  /** append an index to the list. */
  addDatum(datum: number): void {
    this.data = new Link(this.data, datum);
  }

  /** Extend the current prefix using the given branch.
   *  Return the Node representing the extended prefix.
   */
  grow(branch: number, arity: number): TreeNode {
    return this.children[branch] ?? (this.children[branch] = new TreeNode(this, branch));
  }

  /** for debugging purposes only */
  show(pad: string): void {
    this.data?.show(pad + "LIST ");

    this.children.forEach((c, i) => {
      console.log(pad + "Child " + i);
      c.show(pad + " ");
      console.log(pad + "EndChild " + i);
    });
  }
}
