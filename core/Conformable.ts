import Chu from "./Chu";
import Context from "./Context";

export default interface Conformable {
    conform(context: Context): Chu;
}