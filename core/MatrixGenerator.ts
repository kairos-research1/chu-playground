import Chu from "./Chu";
import Link from "./Link";
import Tree from "./Tree";
import TreeNode from "./TreeNode";

/* Generates all matrixes whose rows and columns are taken
 * from the prefix trees passed at construction time.
 * 
 * After construction, each successful call to next
 * produces a representation of a new matrix.
 */
export default class MatrixGenerator {
    // Shape of matrix
    public nrows: number;
    public ncols: number;
    public K: number; // Entries of matrix are in 0...K-1

    // The search algorithm works by trial extension of a region
    // of overlapping partial rows and columns.  The two arrays 
    // below represent that region by locating the Node for
    // each partial row and column.
    private rowNodes: (TreeNode | null)[];
    private colNodes: (TreeNode | null)[];

    // more search variables:  current(Row/Col/Branch)
    // these variables give the cell we are trying to fill,
    // and the value we are trying to fill it with.
    private currentRow: number = 0;
    private currentCol: number = 0;
    private currentBranch: number = 0;

    // if done is true then there are no more matricies
    private done: boolean = false;

    // These arrays represent a matrix.  (The arrays point
    // to lists of indexes of lines that form the matrix.)
    // After a successful call to next(), the caller
    // can examine these arrays to extract the matrix.
    public rowLinks: (Link | null)[] = [];
    public colLinks: (Link | null)[] = [];

    /* Constructor */

    constructor(
        rowTree: Tree, // prefix tree of rows
        colTree: Tree, // prefix tree of columns 
    ) {
        this.ncols = rowTree.length;
        this.nrows = colTree.length;
        this.K = rowTree.arity;

        // initialize row and column search arrays
        this.rowNodes = Array.from({ length: this.nrows }, () => rowTree.top);
        this.colNodes = Array.from({ length: this.ncols }, () => colTree.top);
    }

    /** Try to find the next morphism  
     *  If there is no such morphism, return false  
     *  If there is such a morphism,  
     *  put lists of the possible rows and columns into rowLinks and colLinks,  
     *  then return true.  
     */
    next(): boolean {
        // Loop Invarients:  
        // The prefixes represented by rowNodes and colNodes cover the same set of cells and match in all values.  
        // This set of cells is always the interval before some cell in the following "herringbone" order:  
        //     1  2  3  4  
        //     5  9 10 11  
        //     6 12 15 16  
        //     7 13 17 19  
        //     8 14 18 20  

        if (this.done) return false;

        // Outer loop: drive search forward, extending matrix,
        // check for when we go out of bounds.
        outer: while (this.currentRow < this.nrows && this.currentCol < this.ncols) {
            // Inner loop: go forward one step.
            // Back up as many cells as needed before taking a forward step.
            while (true) {
                // If all possibilities for this cell are exhausted,
                // then back up until it is possible to go forward.
                // If we have to back up and fail, then return false. 
                while (this.currentBranch === this.K) {
                    if (!this.backward()) return false;
                }

                // If we succeed in going forward, then we re-test bounds. 
                // Otherwise we try another value for currentBranch
                if (this.forward()) continue outer;
                else this.currentBranch++;
            }
        }

        // If we get here, the search went out of bounds. 
        // Thus we have a matrix to record.
        this.rowLinks = Array.from(this.rowNodes, (node) => node && node.data);
        this.colLinks = Array.from(this.colNodes, (node) => node && node.data);

        // move search one step beyond this morphism
        // then return true to indicate we have a morphism
        this.backward();
        return true;
    }

    private backward(): boolean {
        // Can't back up from 0,0
        if (this.currentRow === 0 && this.currentCol === 0) {
            this.done = true;
            return false;
        }

        // First step currentRow, currentCol backward
        if (this.currentRow <= this.currentCol) {
            this.currentCol--;  // Shrink a row leftwards

            // If the row is entirely empty,
            //  then go to the end of the previous column.
            if (this.currentRow === this.currentCol + 1)
                this.currentRow = this.nrows - 1;
        }
        else {
            this.currentRow--;  // Shrink a column upwards

            // If the column is entirely empty,
            //  then go to the end of the previous row.
            if (this.currentRow === this.currentCol)
                this.currentCol = this.ncols - 1;
        }

        // Second, restore currentBranch and the prefix trees
        this.currentBranch = (this.rowNodes[this.currentRow]?.branch ?? 0) + 1;
        this.rowNodes[this.currentRow] = this.rowNodes[this.currentRow]?.parent ?? null;
        this.colNodes[this.currentCol] = this.colNodes[this.currentCol]?.parent ?? null;

        return true;  // Report Success  
    }

    private forward(): boolean {
        // Try the current value of branch in the current cell
        const rn = this.rowNodes[this.currentRow]?.child(this.currentBranch);
        const cn = this.colNodes[this.currentCol]?.child(this.currentBranch);

        // If it doesn't work, then report failure
        if (!rn || !cn) return false;

        // First update currentBranch and the prefix trees 
        this.rowNodes[this.currentRow] = rn;
        this.colNodes[this.currentCol] = cn;
        this.currentBranch = 0;

        // Second, step currentRow, currentCol forward
        if (this.currentRow <= this.currentCol) {
            this.currentCol++;  // Grow a row rightward

            // If the row is entirely full,
            //  then go to the start of the next column.
            if (this.currentCol == this.ncols) {
                this.currentCol = this.currentRow;
                this.currentRow = this.currentCol + 1;
            }
        }
        else {
            this.currentRow++;  // Grow a column downward

            // If the column is entirely full,
            //  then go to the start of the next row.
            if (this.currentRow == this.nrows) {
                this.currentRow = this.currentCol + 1;
                this.currentCol = this.currentRow;
            }
        }

        return true; // Report Success
    }

    /** for testing purposes only */
    static main(...args: string[]): void | SyntaxError | TypeError {
        const source = Chu.parse("0000\n0101\n0011\n0110\n", "2", "4", "4");
        if (source instanceof Error) return source;
        const target = Chu.parse("1100\n0110\n0011\n1001\n", "2", "4", "4");
        if (target instanceof Error) return target;

        let rt = target.rowTree();
        rt.show();
        let ct = source.colTree();
        ct.show();

        let G = new MatrixGenerator(rt, ct);
        console.log("made generator");

        let i = 0;
        while (G.next()) {
            i++;
            console.log("Morphism " + i);
            console.log("Row Map");
            G.rowLinks.forEach((l, r) => l?.show("From " + r + " To "));
            console.log("Column Map");
            G.colLinks.forEach((l, c) => l?.show("From " + c + " To "));
            console.log();
        }
    }
}