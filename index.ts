import Chu, { printColClass } from "./core/Chu";
import { throwIfSomeError } from "./util";

try {
    main();
}
catch (err) {
    console.error(err);
}

function main(): void {
    throwIfSomeError([
        Chu.parse("1100\n1010"),
        Chu.parse("000111222\n012012012\n")
    ]).forEach((chu, i, chus) => {
        console.log(`-- chu[${i}] (K = ${chu.K}) --`);
        console.log(chu.serialize());
        console.log();

        console.log(`-- chu[${i}].classifyCols() --`);
        console.log(chu.classifyCols().map(printColClass).join(""));
        console.log();

        for (let j = 0; j <= i; j++) {
            const s = Chu.sequence(chu, chus[j]);
            console.log(`-- sequence(chu[${i}], chus[${j}]) --`);
            console.log(s.serialize());
            console.log();
        }

        const q = chu.query();
        ((s) => {
            if (s instanceof Error) console.error(s); else {
                console.log(`-- q = chu[${i}].query() --`);
                console.log(s);
                console.log();
            }
        })(q.serialize());

        console.log(`-- (j % (q.ncols + 1) === 0) ? "*" : " " --`);
        console.log(Array.from({ length: q.ncols * q.ncols }, (_, j) => (j % (q.ncols + 1) === 0) ? "*" : " ").join(""));
        console.log();

        const impl = Chu.implication(q.dual(), q);
        console.log(`-- q' ⇒ q --`);
        console.log(impl.serialize());
        console.log("------------------------");
    });
}